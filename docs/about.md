---
sidebar_position: 0
---

# About

Nous sommes comme de centaines d'autres débrouillards. Ici, nous partageons ce que nous avons rencontré comme difficultés sous forme de tutoriel pour que vous puissiez ne plus faire la même erreur ou au moins savoir comment résoudre le problème.

Il y a plusieurs sujets, tous mélangés du langage de programmation au github actions en passant par Docker, MQTT (IoT) et les tests en python.

Ainsi la plupart des difficultés rencontrées sont répertoriées [ici](https://soowcode.github.io/) pour en faire un document pour les débrouillards.
