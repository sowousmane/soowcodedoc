// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require("prism-react-renderer/themes/github");
const darkCodeTheme = require("prism-react-renderer/themes/dracula");

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: "👐 Bienvenue à SoowCode 👐",
  tagline: "Cliquez sur cours en haut ou sur le bouton commencez",
  url: 'https://sowousmane.gitlab.io',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/soowcodedoc/',
 // baseUrl: '/soowcodedoc/',
  onBrokenLinks: "throw",
  onBrokenMarkdownLinks: "warn",
  favicon: "img/soowcode.png",

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: "SoowCode", // Usually your GitHub org/user name.
  projectName: "SoowCode docs", // Usually your repo name.

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: "en",
    locales: ["en"],
  },

  presets: [
    [
      "classic",
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve("./sidebars.js"),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          // editUrl:
          //   'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          // editUrl:
          //   'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        theme: {
          customCss: require.resolve("./src/css/custom.css"),
        },
      }),
    ],
  ],
  plugins: [
    [
      require.resolve("@cmfcmf/docusaurus-search-local"),
      {
       language: "fr",
      },
    ],
  ],
  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: "SoowCode",
        logo: {
          alt: "SoowCode Logo",
          src: "img/soowcode.png",
        },
        items: [
          {
            type: "doc",
            docId: "about",
            position: "left",
            label: "Cours",
          },
          // {to: '/blog', label: 'Forum', position: 'left'},
          {
            href: "https://github.com/soowcode",
            label: "GitHub",
            position: "right",
          },
        ],
      },
      footer: {
        style: "dark",
        links: [
          {
            title: "Docs",
            items: [
              {
                label: "Cours",
                to: "/docs/about",
              },
            ],
          },
          {
            title: "Communité",
            items: [
              {
                label: "Stack Overflow",
                href: "https://github.com/soowcode",
              },
              {
                label: "Discord",
                href: "https://github.com/soowcode",
              },
              {
                label: "Twitter",
                href: "https://github.com/soowcode",
              },
            ],
          },
          {
            title: "Plus",
            items: [
              {
                label: "Forum",
                href: "https://gobyexample.com/hello-world",
              },
              {
                label: "GitHub",
                href: "https://github.com/soowcode",
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} SoowCode`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
      algolia: {
        // The application ID provided by Algolia
        appId: "YOUR_APP_ID",

        // Public API key: it is safe to commit it
        apiKey: "YOUR_SEARCH_API_KEY",

        indexName: "YOUR_INDEX_NAME",

        // Optional: see doc section below
        contextualSearch: true,

        // Optional: Specify domains where the navigation should occur through window.location instead on history.push. Useful when our Algolia config crawls multiple documentation sites and we want to navigate with window.location.href to them.
        externalUrlRegex: "external\\.com|domain\\.com",

        // Optional: Algolia search parameters
        searchParameters: {},

        // Optional: path for search page that enabled by default (`false` to disable it)
        searchPagePath: "search",
      },
    }),
};

module.exports = config;
