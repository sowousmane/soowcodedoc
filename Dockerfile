FROM node:lts

WORKDIR /app/my-website

EXPOSE 3000 35729
COPY ./my-website /app/my-website
RUN npm install

CMD ["npm", "run", "start"]
