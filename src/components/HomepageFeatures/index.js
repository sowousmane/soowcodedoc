import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Facile à utiliser',
    Svg: require('@site/static/img/undraw_docusaurus_mountain.svg').default,
    description: (
      <>
        
    SoowCode a été créée dès le départ pour être facilement compris et utilisé pour que votre temps sur le Web soit réduit et que votre productivité soit décuplées.
      </>
    ),
  },
  {
    title: 'Concentrez-vous sur ce qui compte',
    Svg: require('@site/static/img/undraw_docusaurus_tree.svg').default,
    description: (
      <>
        
SoowCode vous permet de vous concentrer sur vos recherches de réponse, et nous ferons les corvées. Allez-y et commencez à apprendre .
      </>
    ),
  },
  {
    title: 'Participer au projet?',
    Svg: require('@site/static/img/undraw_docusaurus_react.svg').default,
    description: (
      <>
Clonez ou forker le projet, faitez une merge request et devenez un passager du voyage.
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
